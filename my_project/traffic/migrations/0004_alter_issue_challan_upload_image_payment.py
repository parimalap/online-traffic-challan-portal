# Generated by Django 4.1.5 on 2023-03-14 06:13

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('traffic', '0003_alter_issue_challan_upload_image'),
    ]

    operations = [
        migrations.AlterField(
            model_name='issue_challan',
            name='upload_image',
            field=models.ImageField(blank=True, null=True, upload_to='images/'),
        ),
        migrations.CreateModel(
            name='Payment',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('payment_amount', models.IntegerField()),
                ('payment_date', models.DateField(auto_now_add=True)),
                ('payment_method', models.CharField(max_length=50)),
                ('challan', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, to='traffic.issue_challan')),
            ],
        ),
    ]
