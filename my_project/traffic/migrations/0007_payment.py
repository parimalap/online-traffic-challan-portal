# Generated by Django 4.1.5 on 2023-03-15 04:00

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('traffic', '0006_alter_issue_challan_issued_by'),
    ]

    operations = [
        migrations.CreateModel(
            name='Payment',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('vehicle_number', models.CharField(max_length=20)),
                ('amount_paid', models.DecimalField(decimal_places=2, max_digits=10)),
                ('date_of_payment', models.DateTimeField(auto_now_add=True)),
            ],
        ),
    ]
